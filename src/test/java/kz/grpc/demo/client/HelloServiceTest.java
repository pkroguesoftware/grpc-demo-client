package kz.grpc.demo.client;

import hello.HelloReply;
import hello.HelloRequest;
import kz.grpc.demo.client.service.HelloService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class HelloServiceTest {

    @Autowired
    HelloService helloService;

    @Test
    void sayHello() {
        List<String> names = Arrays.asList("Avetik", "Seilbek");
        names.forEach(name -> {
            HelloRequest request = makeRequest(name);
            HelloReply reply = helloService.hello(request);

            assertNotNull(reply, "Reply must not be null");
            assertTrue(reply.getGreeting().contains(name), "Reply must contain passed name");
            System.out.printf("Request name = '%s', Reply = '%s'%n", request.getName(), reply.getGreeting());
        });
    }


    @Test
    void sayHelloAgain() {
        List<String> names = Arrays.asList("Avetik", "Seilbek");
        names.forEach(name -> {
            HelloRequest request = makeRequest(name);
            HelloReply reply = helloService.helloAgain(request);

            assertNotNull(reply, "Reply must not be null");
            assertTrue(reply.getGreeting().contains(name), "Reply must contain passed name");
            System.out.printf("Request name = '%s', Reply = '%s'%n", request.getName(), reply.getGreeting());
        });
    }

    private HelloRequest makeRequest(String name) {
        return HelloRequest.newBuilder().setName(name).build();
    }

}
