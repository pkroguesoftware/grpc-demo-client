package kz.grpc.demo.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientApplication.class, args);
	}

	@Bean
	public ManagedChannel getChannel(){
		return ManagedChannelBuilder
				.forTarget("localhost:8080")
				.usePlaintext()
				.build();
	}
}
