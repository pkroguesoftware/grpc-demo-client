package kz.grpc.demo.client.service;

import grpc.hello.HelloGrpc;
import hello.HelloReply;
import hello.HelloRequest;
import io.grpc.ManagedChannel;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class HelloServiceImpl implements HelloService {
    private final @NonNull ManagedChannel channel;

    public HelloServiceImpl(@NonNull ManagedChannel channel) {
        this.channel = channel;
    }

    @Override
    public @NonNull HelloReply hello(@NonNull HelloRequest helloRequest) {
        HelloGrpc.HelloBlockingStub blockingStub = HelloGrpc.newBlockingStub(channel);
        return blockingStub.hello(helloRequest);
    }


    @Override
    public @NonNull HelloReply helloAgain(@NonNull HelloRequest helloRequest) {
        HelloGrpc.HelloBlockingStub blockingStub = HelloGrpc.newBlockingStub(channel);
        return blockingStub.helloAgain(helloRequest);
    }
}
