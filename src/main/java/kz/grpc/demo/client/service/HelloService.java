package kz.grpc.demo.client.service;

import hello.HelloReply;
import hello.HelloRequest;
import lombok.NonNull;

public interface HelloService {
    @NonNull HelloReply hello(@NonNull HelloRequest helloRequest);
    @NonNull HelloReply helloAgain(@NonNull HelloRequest helloRequest);
}
